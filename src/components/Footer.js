import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer id='footer' className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-md-6 col-12"}>
                        <h1>
                            Trefle
                        </h1>
                    </div>
                    <div className={"col-md-6 col-12"}>
                        <p className={"text-light"}>
                            This website is based on <a href={"https://trefle.io/"}>Trefle free API</a> for a school project<br />
                            Developped by <a href={"syriane-bonjean.fr"} >Syriane Bonjean.</a>
                        </p>
                    </div>
                </div>
            </footer>
        );
    }
}
export default Footer;