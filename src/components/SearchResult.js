import React from 'react';
import SavePlant from "./SavePlant";
import {
    Link
} from 'react-router-dom';

function SearchResult (props){

    if (props.plant.common_name == null){
        return null;
    }
    return (
        <>
            <ul className="list-group col-6 col-md-4 mt-4">
                <li className="list-group-item text-center btn btn-outline-dark">
                    <h5>
                        <Link to={`/info-plant/${props.plant.id}`}>
                            -{props.plant.common_name}-
                        </Link>
                    </h5>
                </li>
                <li className="list-group-item text-center">
                    <em>
                        {props.plant.scientific_name}
                    </em>
                </li>
                <SavePlant class={"list-group-item text-center align-middle pointer"} setLikedPlants={props.setLikedPlants} likedPlants={props.likedPlants} />
            </ul>
        </>
    );
}
export default SearchResult;