import React from 'react';
import SavePlant from "./SavePlant";

class PlantInfo extends React.Component{
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.plant);
        return(
            <>
                <ul className="list-group shadow">
                    {this.props.plant.images.length > 0 &&
                    <li className="list-group-item">
                        <img className={'img-fluid'} src={this.props.plant.images[0].url}/>
                    </li>
                    }
                    <li className="list-group-item">
                        <h2>
                            {this.props.plant.common_name}
                        </h2>
                    </li>
                    <li className="list-group-item disabled">
                        <em>
                            {this.props.plant.scientific_name}
                        </em>
                    </li>
                    {this.props.plant.division &&
                    <li className="list-group-item">
                        Division : {this.props.plant.division.name}
                    </li>
                    }
                    {this.props.plant.class &&
                    <li className="list-group-item">
                        Division class : {this.props.plant.class.name}
                    </li>
                    }
                    {this.props.plant.order &&
                    <li className="list-group-item">
                        Division order : {this.props.plant.order.name}
                    </li>
                    }
                    {this.props.plant.family &&
                    <li className="list-group-item">
                        Family : {this.props.plant.family.name}
                    </li>
                    }
                    {this.props.plant.genus &&
                    <li className="list-group-item">
                        Genus : {this.props.plant.genus.name}
                    </li>
                    }
                    <SavePlant class={"list-group-item text-center align-middle pointer"} setLikedPlants={this.props.setLikedPlants} likedPlants={this.props.likedPlants}/>
                </ul>
            </>
        );
    }
}
export default PlantInfo;