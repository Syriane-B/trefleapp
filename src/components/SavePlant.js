import React from 'react';
import SearchResult from "./SearchResult";

function SavePlant(props) {
    const saveToFavorite = (event, plantId) => {
        props.setLikedPlants(plantId);
    };

    // plants.map((plant, index) => (
    //     <SearchResult plant={plant} key={index} likedPlants={props.likedPlants}/>
    // ))

    props.likedPlants.map((plantId) => {
        if (plantId === props.plant.id){
            return (
                <li className={props.class}>
                    ...unsave...<br/>
                    <i className="material-icons text-danger" onClick={event => saveToFavorite(event, props.plant.id)} >favorite_border</i>
                </li>
            );
        }
        else {
            return (
                <li className={props.class}>
                    save it ! <br/>
                    <i className="material-icons text-danger" onClick={event => saveToFavorite(event, props.plant.id)} >favorite_border</i>
                </li>
            );
        }
    });
    return <></>;
}

export default SavePlant;