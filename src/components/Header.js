import React from 'react';
import {
    Link
} from 'react-router-dom';

class Header extends React.Component {
    render() {
        return (
            <header id='header' className={"container-fluid bigHeader"}>
                <h1>
                    Trefle
                </h1>
                <div className={"float-right mr-5 mb-5"}>
                    <Link to={"/"} className={"mr-2"}>
                        <i className="material-icons text-light">home</i>
                    </Link>
                    <Link to={"/search"}>
                        <i className="material-icons text-light">search</i>
                    </Link>
                    <Link to={"/my-plant"} className={"ml-2"}>
                        <i className="material-icons text-danger">favorite_border</i>
                    </Link>
                </div>
            </header>
        );
    }
}
export default Header;