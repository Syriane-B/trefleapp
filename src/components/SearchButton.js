import React from 'react';
import {
    Link
} from 'react-router-dom';

function SearchButton(props) {
    return (
            <p className={'searchButton text-center'}>
                <Link to={'/search'}>Look for a plant ?</Link>
            </p>
    );
}

export default SearchButton;