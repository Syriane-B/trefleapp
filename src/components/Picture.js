import React from 'react';

class Picture extends React.Component{
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
    }
    render() {
        return (

                    <div className="imgBloc">
                        <div className="circle">
                            {/*<img src={this.props.url} className={this.props.size} alt="plantPicture">*/}
                            <img src={this.props.img} className="" alt="plantPicture">
                            </img>
                        </div>
                        <div className="colorBloc"></div>
                    </div>

        );
    }

}
export default Picture;