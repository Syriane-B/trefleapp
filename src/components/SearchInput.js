import React, {useState} from 'react';
import {
    Link
} from 'react-router-dom';
function SearchInput (props) {
const [textValue, setTextValue] = useState('');
    //set dynamically textValue state with the input value
    const handleInputChange = (e) => {
        setTextValue(e.target.value);
    };
    //send the textValue state to the parent via handleNewSearch function
    const handleSubmit = (e) => {
        e.preventDefault();
    };
        return (
        <div className="container">
          <form className="row" onSubmit={handleSubmit}>
                <input onChange={handleInputChange} className="col-6 form-control" type="text" id="searchInput" aria-describedby="plant search input" placeholder="Look for a plant">
                </input>
                <Link to={`/search/${textValue}`} className={"col-2"}>
                    <button className="btn btn-outline-dark" type="submit">SearchPage</button>
                </Link>
            </form>
        </div>
        );
}
export default SearchInput;