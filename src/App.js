import React, {useState} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import Footer from "./components/Footer";
import PlantInfoPage from "./pages/PlantInfoPage";
import SearchPage from "./pages/SearchPage";
import HomePage from "./pages/HomePage";
import MyPlantLiked from "./pages/MyPlantLiked";
import SearchPageResult from "./pages/SearchPageResult";

function App (){
    //TODO : Remove fake datas in liked plant array
    const [likedPlants, setLikedPlants] = useState([]);
    //function to catch plant id when user save a new plant (on search results or plant info page) and save it into state
    const handleNewLikedPlant = (plantId) =>{
        setLikedPlants([...likedPlants, plantId]);
    };
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route path="/" component={HomePage}>
                        <Route path={"/info-plant/:plantId"} component={() => <PlantInfoPage likedPlants={likedPlants}/>} />
                        <Route path='/search/:searchText' component={() => <SearchPageResult setLikedPlants={handleNewLikedPlant} likedPlants={likedPlants}/>} />
                        <Route path='/search' component={() => <SearchPage />} />
                        <Route path='/my-plant' component={() => <MyPlantLiked likedPlants={likedPlants}/>} />
                    </Route>
                </Switch>
                <Footer/>
            </div>
        </Router>
    )
}
export default App;