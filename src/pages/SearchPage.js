import React from 'react';
import SearchInput from '../components/SearchInput';
import HeaderSmall from "../components/HeaderSmall";

function SearchPage (props) {

    return (
        <>
            <HeaderSmall/>
            <div id="content" className="margin-top pb-5">
                <div className="container">
                    <SearchInput />
                </div>
            </div>
        </>
    );
}
export default SearchPage;
