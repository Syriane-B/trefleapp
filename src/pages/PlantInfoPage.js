import React, { useState, useEffect } from 'react';
import PlantInfo from "../components/PlantInfo";
import HeaderSmall from "../components/HeaderSmall";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
} from "react-router-dom";
const axios = require('axios');


export default function PlantInfoPage (props) {
    const [stop, setStop] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [plant, setPlant] = useState([]);
    let { plantId } = useParams();

    useEffect(() => {
        axios.get(`https://trefle.io/api/plants/${plantId}?token=eTlsSWVwVnhtbUo0Z2FmK1hnN29sUT09`)
            .then((response) => {
                setPlant(response.data);
            })
            .catch((error) => {
                // handle error
                console.log(error);
            })
            .finally(() => {
                // always executed
                setIsLoaded( true);
            });
    }, [stop]);

    if (isLoaded){
        return(
            <>
                <HeaderSmall/>
                <div id="content" className="mt-3 pb-5">
                    <div className="container">
                        <PlantInfo plant={plant} likedPlants={props.likedPlants}/>
                    </div>
                </div>
            </>
        )
    }
    else {
        return(
            <>
                <HeaderSmall/>
                <div id="content" className="margin-top pb-5">
                    <div className="container">
                        <div>loading</div>
                    </div>
                </div>
            </>
        )
    }
}