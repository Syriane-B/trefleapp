import React, {useState} from 'react';
import SearchInput from '../components/SearchInput';
import SearchResult from "../components/SearchResult";
import HeaderSmall from "../components/HeaderSmall";
import {
    useParams
} from "react-router-dom";

const axios = require('axios');

function SearchPageResults (props) {
    const [plants, setPlants] = useState([]);
    let plantSearch = useParams();
    if (plantSearch) {
        axios.get(`https://trefle.io/api/plants?q=${plantSearch}&token=eTlsSWVwVnhtbUo0Z2FmK1hnN29sUT09`)
            .then((response) => {
                // set datas array state
                setPlants(response.data);
            })
            .catch((error) => {
            })
    }
    return (
        <>
            <HeaderSmall/>
            <div id="content" className="margin-top pb-5">
                <div className="container">
                    <SearchInput/>
                    <div className="card-deck">
                        {plants.map((plant, index) => (
                            <SearchResult plant={plant} key={index} setLikedPlants={(e)=>{props.setLikedPlants(e)}}/>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}
export default SearchPageResults;
