import React, {useState, useEffect} from 'react';
import HeaderSmall from "../components/HeaderSmall";
import SearchResult from "../components/SearchResult";

const axios = require('axios');
function MyPlantLiked (props) {
    const [plants, setPlants] = useState([]);

    useEffect(() => {
        if(props.likedPlants.length > 0){
            props.likedPlants.map((plantId) => (
                axios.get(`https://trefle.io/api/plants/${plantId}?token=eTlsSWVwVnhtbUo0Z2FmK1hnN29sUT09`)
                    .then((response) => {
                        // set datas array state setSearches(searches => [...searches, query])
                        setPlants(plants => [...plants, response.data]);
                    })
                    .catch((error) => {
                        console.log(error)
                    })
            ));
        }
    }, [props.likedPlants]);
    if(plants.length == 0){
        return (
            <>
                <HeaderSmall/>
                <div className={"container mt-3"}>
                    <h2>MyPlantLikedPage</h2>
                    <div className="card-deck">
                        <p className={"margin-top"}>You didn't save plants yet !</p>
                    </div>
                </div>
            </>
        );
    }
    else {
        return (
            <>
                <HeaderSmall/>
                <div className={"container mt-3"}>
                    <h2>MyPlantLikedPage</h2>
                    <div className="card-deck">
                        {plants.map((plant, index) => (
                        <SearchResult plant={plant} key={index} likedPlants={props.likedPlants}/>
                        ))}
                    </div>
                </div>
            </>
        );
    }
}

export default MyPlantLiked;