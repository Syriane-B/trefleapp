import React, {useState, useEffect} from 'react';
import SearchInput from '../components/SearchInput';
import SearchResult from "../components/SearchResult";
import HeaderSmall from "../components/HeaderSmall";
import { useParams } from "react-router-dom";

const axios = require('axios');

function SearchPageResult (props) {
    let { searchText } = useParams();
    const [plants, setPlants] = useState([]);

    useEffect(() => {
        if(searchText !== ''){
            axios.get(`https://trefle.io/api/plants?q=${searchText}&token=eTlsSWVwVnhtbUo0Z2FmK1hnN29sUT09`)
                .then((response) => {
                    // set datas array state
                    setPlants(response.data);
                })
                .catch((error) => {
                })
        }
    }, [searchText]);

    return (
        <>
            <HeaderSmall/>
            <div id="content" className="mt-3 pb-5">
                <div className="container">
                    <SearchInput/>
                    <div className="card-deck">
                        {plants.map((plant, index) => (
                            <SearchResult plant={plant} key={index} setLikedPlants={props.setLikedPlants} likedPlants={props.likedPlants}/>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}
export default SearchPageResult;
