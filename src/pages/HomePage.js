import React from 'react';
import Header from "../components/Header";
import SearchButton from "../components/SearchButton";

function HomePage(props) {
    return (
        <>
            <Header/>
            <div id={"content"}>
                <div className={"container"}>
                    <div className={"row margin-top"}>
                        <div className={"col-12 col-sm-6"}>
                            <h2>
                                What is Trefle ?
                            </h2>
                            <p>
                                This website allow you to get information on plant. You can get the scientific name from a plant, its needs as well as
                                its classification.
                            </p>
                        </div>
                        <div className={"col-6 col-sm-4"}>
                            <SearchButton/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default HomePage;